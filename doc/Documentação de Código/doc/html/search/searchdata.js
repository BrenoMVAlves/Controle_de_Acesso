var indexSectionsWithContent =
{
  0: "_bcdkmprs",
  1: "k",
  2: "km",
  3: "_dkms",
  4: "cdkpr",
  5: "k",
  6: "k",
  7: "k",
  8: "bk"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Pages"
};

