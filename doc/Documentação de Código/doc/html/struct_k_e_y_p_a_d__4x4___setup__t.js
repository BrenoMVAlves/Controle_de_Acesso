var struct_k_e_y_p_a_d__4x4___setup__t =
[
    [ "COL1", "struct_k_e_y_p_a_d__4x4___setup__t.html#a041a739e50176db84102661dfeab9838", null ],
    [ "COL2", "struct_k_e_y_p_a_d__4x4___setup__t.html#aee4ba9c96c13c81bd4ba1134e091341d", null ],
    [ "COL3", "struct_k_e_y_p_a_d__4x4___setup__t.html#a331980ca8d0efbf90d02809cb4eb6e50", null ],
    [ "COL4", "struct_k_e_y_p_a_d__4x4___setup__t.html#abcf8063b7da5953f7aed8a184fe6c291", null ],
    [ "Pin", "struct_k_e_y_p_a_d__4x4___setup__t.html#a38b1073a17d7b10e6c87f0d97c0e8ab6", null ],
    [ "Port", "struct_k_e_y_p_a_d__4x4___setup__t.html#a6793c55dcc9386b4e76a7d311615eadc", null ],
    [ "ROW1", "struct_k_e_y_p_a_d__4x4___setup__t.html#aa50e4f3ecd938d826e374e261b1845c8", null ],
    [ "ROW2", "struct_k_e_y_p_a_d__4x4___setup__t.html#ab64bd25ffaf01adf5a9ed1eb43f3f0be", null ],
    [ "ROW3", "struct_k_e_y_p_a_d__4x4___setup__t.html#a4a04f18c396ab458c71d68e5e7d454dc", null ],
    [ "ROW4", "struct_k_e_y_p_a_d__4x4___setup__t.html#a6f8e2e56f1cad7780f3ddba146ad70b0", null ]
];