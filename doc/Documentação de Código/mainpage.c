/**
*
*\mainpage
*
******************************************************************************
* \attention
*
* file in development
* 
******************************************************************************
* \section introduction Introduction
*
* The purpose of this activity is to implement a code capable of receiving values typed in a matrix keyboard. 
* Since this keyboard can be used for several applications, the focus of this work is the development of a 
* library to decouple the application code and facilitate its reuse.
* The target model is the 4x4 Matrix Membrane Keypad from Parallax company shown in the figure below.
* Matrix keyboards use a combination of four rows and fourcolumns to 
* provide button states to the host device, typically a microcontroller. Each key is a button, with *one end 
* connected to one line and the other end connected to a column.
*
*
*
* \image html keypad4x4.jpg "teclado matricial 4x4"
*
* 
* \section library Library
* 
* the implemented libraries are: 
*
* \li\ref keypadh
* \li\ref keypadc
*
*/