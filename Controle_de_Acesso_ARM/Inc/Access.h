//*---------------------------------------------------------------------------*/
/** \file Access.h
 * \page Access Access.h
 *
 * \author Breno Alves
 * \n breno.alves@ee.ufcg.edu.br

 * \version 0.1
 * \date Creation: 01 mar 2018.
 * \date Last Update: 15 mar 2018.
 *
 * *****************************************************************************
*/


/*---------------------------------------------------------------------------*/
/*---------------------- Evitar a inclus�o recursiva ------------------------*/
#ifndef ACCESS_H_
#define ACCESS_H_

/*---------------------------------------------------------------------------*/
/*-------------------- Defines, Constantes e Macros -------------------------*/
#define CPF_SIZE 11
#define PASSWORD_SIZE 4
#define MAX_USERS 50
#define MAX_LOG 300

/*---------------------------------------------------------------------------*/
/*----------------------------- Includes ------------------------------------*/
#include "stdio.h"
#include "time.h"

/*---------------------------------------------------------------------------*/
/*------------------------- Vari�veis e tipos -------------------------------*/

/**
 * \enum  statusAccess_t
 * \brief Define os status de Acesso.*/
typedef enum
{
	Waiting_CPF_status = 0x00,
	Waiting_Password_status = 0x01,
}statusAccess_t;

/**
 * \enum  resultAccess_t
 * \brief Define os resultados de Acesso.*/
typedef enum
{
	Access_Allowed_result = 0x00,
	Access_Denied_result = 0x01,
	Access_No_result = 0x02
}resultAccess_t;

/**
 * \struct  userInfo_t
 * \brief Define o tipo com as informa��es do usu�rio..*/
typedef struct
{
	char* Name;
	char Password[PASSWORD_SIZE];
	char CPF[CPF_SIZE];
}userInfo_t;

/**
 * \struct  logAccess_t
 * \brief Define o tipo com as informa��es do log.*/
typedef struct
{
	char* UserCPF;
	struct tm Time;
}logAccess_t;

/*---------------------------------------------------------------------------*/
/*------------------------- Fun��es p�blicas --------------------------------*/
void Access_Init();
void Access_ReadCPF(uint8_t pressedKey);
void Access_ReadPassword(uint8_t pressedKey);
statusAccess_t Access_GetStatus();
resultAccess_t Access_GetResult();
char * Access_GetPartialCPF();
char * Access_GetUserName();



#endif /* ACCESS_H_ */



