/*---------------------------------------------------------------------------*/
/**\file grove_lcd_rgb_v2.h
 * \page grove_lcd_rgb_v2 Grove LCD RGB Backlight V2.
 *
 * \section descricao Descri��o
 *
 * Biblioteca para controle do Grove LCD RGB Backlight V2.
 *
 * \subsection arquivo Arquivo
 * \ref grove_lcd_rgb_v2.h \n
 * \ref grove_lcd_rgb_v2.c
 *
 * \section resumo Resumo
 * \author Charles Dias
 * \n christian.dias@ee.ufcg.edu.br ou
 * \n charlesdias@tecnofuturo.com.br
 * \version 0.1
 * \date Cria��o: 06/12/2017.
 * \date Altera��o: 06/12/2017.
 *
 * *****************************************************************************
 * \attention
 *
 * *****************************************************************************
 *
 * *****************************************************************************
 * \bug
 *
 * *****************************************************************************
 */

/*---------------------------------------------------------------------------*/
/*---------------------- Evitar a inclus�o recursiva ------------------------*/
#ifndef GROVE_LCD_RGB_V2_H_
#define GROVE_LCD_RGB_V2_H_

/*---------------------------------------------------------------------------*/
/*----------------------------- Includes ------------------------------------*/
#include <stdbool.h>
#include <inttypes.h>
#include "stm32f4xx_hal.h"
//#include "stm32f4xx_hal_i2c.h"
//#include "i2c.h"
//#include "gpio.h"

/*---------------------------------------------------------------------------*/
/*-------------------- Defines, Constantes e Macros -------------------------*/

/** Device I2C Address for LCD.*/
#define LCD_ADDRESS     					(0x7C)

/** Device I2C Address for Backlight.*/
#define RGB_ADDRESS     					(0xC4)

/*---------------------------------------------------------------------------*/
/*------------------------- Vari�veis e tipos -------------------------------*/

/**
 * \enum  LCD_status_t
 * \brief Define status de retorno para fun��es do LCD.*/
typedef enum
{
  LCD_OK       				= 0x00U,	/**< */
  LCD_ERROR   				= 0x01U,
  LCD_BUSY     				= 0x02U,
  LCD_TIMEOUT  				= 0x03U,
} LCD_status_t;


/**
 * \enum  lcd_row_t
 * \brief Define o n�mero de linhas do LCD.*/
typedef enum
{
    LCD_ROW_01			 	= 0x00U,	/**< */
	LCD_ROW_02				= 0x08U, 	/**< */
}lcd_row_t;


/**
 * \enum  lcd_backlight_t
 * \brief Define cor do backlight.*/
typedef enum
{
	LCD_BACKLIGHT_OFF	 	= 0x00U,	/**< */
	LCD_BACKLIGHT_WHITE 	= 0x01U,	/**< */
	LCD_BACKLIGHT_RED		= 0x02U, 	/**< */
	LCD_BACKLIGHT_GREEN		= 0x03U, 	/**< */
	LCD_BACKLIGHT_BLUE		= 0x04U,	/**< */
}lcd_backlight_t;


/**
 * \struct  lcd_grove_t
 * \brief Define estrutura de dados para Grove LCD RGB.*/
typedef struct
{
	bool b_lcd_init;					/**< */
	bool b_backlight_init;				/**< */
	uint8_t ui8_address_lcd;			/**< */
	uint8_t ui8_address_rbg;			/**< */
	uint8_t ui8_timeout;				/**< */
	I2C_HandleTypeDef* p_i2c_base;		/**< */
}lcd_grove_t;


/*---------------------------------------------------------------------------*/
/*------------------------- Fun��es p�blicas --------------------------------*/

LCD_status_t LCD_init(lcd_grove_t* me, I2C_HandleTypeDef* p_i2c_base, lcd_row_t rows);
LCD_status_t LCD_home(lcd_grove_t* me);
LCD_status_t LCD_clear(lcd_grove_t* me);
LCD_status_t LCD_set_cursor(lcd_grove_t* me, uint8_t col, uint8_t row);
LCD_status_t LCD_write_char(lcd_grove_t* me, uint8_t ui8_value);
void LCD_print(lcd_grove_t me, char* message, uint8_t col, uint8_t row );
void LCD_createChar(lcd_grove_t* me,uint8_t location, uint8_t charmap[]);

LCD_status_t LCD_BACKLIGHT_init(lcd_grove_t* me, I2C_HandleTypeDef* p_i2c_base);
LCD_status_t LCD_BACKLIGHT_set_rgb(lcd_grove_t* me, uint8_t ui8_red, uint8_t ui8_green, uint8_t ui8_blue);
LCD_status_t LCD_BACKLIGHT_set_color(lcd_grove_t* me, lcd_backlight_t color);

#endif /* GROVE_LCD_RGB_V2_H_ */
