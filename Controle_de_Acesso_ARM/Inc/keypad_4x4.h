
 /*---------------------------------------------------------------------------*/
/** \file keypad.h
 * \page keypadh keypad_4x4 Hearder File.
 * 
 * \author Breno Alves
 * \n breno.alves@ee.ufcg.edu.br
 * \author Inaie ALves
 * \n inaie.soares@ee.ufcg.edu.br
 * \author Vinicius Freire  
 * \n vinicius.bezerra@ee.ufcg.edu.br
 * \version 0.1
 * \date Creation: 14 nov 2017.
 * \date Last Update: 23  nov 2017.
 *
 * *****************************************************************************
 * \bug 
 * As a consequence of the debaunce treatment the key reading is passed to the
 * system only when the key is deactivated.
 *
 * *****************************************************************************
 *
 */

#ifndef KEYPAD_4X4_H_
#define KEYPAD_4X4_H_

#include "stdio.h"
#include "gpio.h"

/* Keypad no pressed */
#define KEYPAD_NO_PRESSED 0xFFU

/**
 * @brief  Keypad Setup  struct
 * @Description Structure that defines ports and pins to each row and column
 */
typedef struct{
	struct{
		GPIO_TypeDef * Port;
		uint16_t Pin;
	}ROW1,ROW2,ROW3,ROW4,COL1,COL2,COL3,COL4;
}KEYPAD_4x4_Setup_t;


/**
 * @brief  Keypad button enumeration
 */
typedef enum {
	KEYPAD_4x4_Button_0 = 0x00,                     /*!< Button 0 code */
	KEYPAD_4x4_Button_1 = 0x01,                     /*!< Button 1 code */
	KEYPAD_4x4_Button_2 = 0x02,                     /*!< Button 2 code */
	KEYPAD_4x4_Button_3 = 0x03,                     /*!< Button 3 code */
	KEYPAD_4x4_Button_4 = 0x04,                     /*!< Button 4 code */
	KEYPAD_4x4_Button_5 = 0x05,                     /*!< Button 5 code */
	KEYPAD_4x4_Button_6 = 0x06,                     /*!< Button 6 code */
	KEYPAD_4x4_Button_7 = 0x07,                     /*!< Button 7 code */
	KEYPAD_4x4_Button_8 = 0x08,                     /*!< Button 8 code */
	KEYPAD_4x4_Button_9 = 0x09,                     /*!< Button 9 code */
	KEYPAD_4x4_Button_STAR = 0x0A,                  /*!< Button STAR code */
	KEYPAD_4x4_Button_HASH = 0x0B,                  /*!< Button HASH code */
	KEYPAD_4x4_Button_A = 0x0C,	                    /*!< Button A code. */
	KEYPAD_4x4_Button_B = 0x0D,	                    /*!< Button B code. */
	KEYPAD_4x4_Button_C = 0x0E,	                    /*!< Button C code. */
	KEYPAD_4x4_Button_D = 0x0F,	                    /*!< Button D code. */
	KEYPAD_4x4_Button_NOPRESSED = KEYPAD_NO_PRESSED /*!< No button pressed */
} KEYPAD_4x4_ButtonEnum_t;


/**
 * @brief  Keypad Init function
 * @description Initializes rows and columns ports and pins
 * @param A @ref KEYPAD_4x4_Setup struct reference
 * @returns void
 */
void KEYPAD_4x4_Init(KEYPAD_4x4_Setup_t*);


/**
 * @brief  Keypad GetPressedKey function
 * @description Get the pressed key from keypad
 * @param void
 * @returns A KEYPAD_4x4_ButtonEnum_t type with the value of the pressed key
 */
KEYPAD_4x4_ButtonEnum_t KEYPAD_4x4_GetPressedKey(void);


#endif /* KEYPAD_4X4_H_ */










