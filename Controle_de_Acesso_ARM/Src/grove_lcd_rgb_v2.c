/*---------------------------------------------------------------------------*/
/** \file grove_lcd_rgb_v2.c
 *
 * \author Charles Dias
 * \n christian.dias@ee.ufcg.edu.br ou
 * \n charlesdias@tecnofuturo.com.br
 * \version 0.1
 * \date Cria��o: 06/12/2017.
 * \date Altera��o: 06/12/2017.
 *
 */


/*---------------------------------------------------------------------------*/
/*----------------------------- Includes ------------------------------------*/
#include "grove_lcd_rgb_v2.h"
#include "assert.h"
#include "string.h"
/*---------------------------------------------------------------------------*/
/*-------------------- Defines, Constantes e Macros -------------------------*/

#define REG_RED         			0x04        // pwm2
#define REG_GREEN       			0x03        // pwm1
#define REG_BLUE        			0x02        // pwm0

#define REG_MODE1       			0x00
#define REG_MODE2       			0x01
#define REG_OUTPUT      			0x08

// commands
#define LCD_CLEAR_DISPLAY 			0x01
#define LCD_RETURN_HOME 			0x02
#define LCD_ENTRY_MODE_SET 			0x04
#define LCD_DISPLAY_CONTROL 		0x08
#define LCD_CURSOR_SHIFT 			0x10
#define LCD_FUNCTION_SET 			0x20
#define LCD_SET_CGRAM_ADDR 			0x40
#define LCD_SET_DDRAM_ADDR 			0x80

// flags for display entry mode
#define LCD_ENTRY_RIGHT 			0x00
#define LCD_ENTRY_LEFT 				0x02
#define LCD_ENTRY_SHIFT_INCREMENT 	0x01
#define LCD_ENTRY_SHIFT_DECREMENT 	0x00

// flags for display on/off control
#define LCD_DISPLAY_ON 				0x04
#define LCD_DISPLAY_OFF 			0x00
#define LCD_CURSOR_ON 				0x02
#define LCD_CURSOR_OFF 				0x00
#define LCD_BLINK_ON 				0x01
#define LCD_BLINK_OFF 				0x00

// flags for display/cursor shift
#define LCD_DISPLAY_MOVE	 		0x08
#define LCD_CURSOR_MOVE				0x00
#define LCD_MOVE_RIGHT	 			0x04
#define LCD_MOVE_LEFT 				0x00


/*---------------------------------------------------------------------------*/
/*--------------------- Tipos e vari�veis privadas --------------------------*/

const uint8_t ui8_color_define[][3] =
{
	{0, 0, 0},            			// off
	{255, 255, 255},            	// white
    {255, 0, 0},                	// red
    {0, 255, 0},                	// green
    {0, 0, 255},                	// blue
};

/*---------------------------------------------------------------------------*/
/*-------------------- Prop�tipos de fun��es p�blicas -----------------------*/
LCD_status_t LCD_init(lcd_grove_t* me, I2C_HandleTypeDef* p_i2c_base, lcd_row_t rows);
LCD_status_t LCD_home(lcd_grove_t* me);
LCD_status_t LCD_clear(lcd_grove_t* me);
LCD_status_t LCD_set_cursor(lcd_grove_t* me, uint8_t col, uint8_t row);
LCD_status_t LCD_write_char(lcd_grove_t* me, uint8_t ui8_value);
void LCD_print(lcd_grove_t me, char* message, uint8_t col, uint8_t row );
void LCD_createChar(lcd_grove_t* me,uint8_t location, uint8_t charmap[]);

LCD_status_t LCD_BACKLIGHT_init(lcd_grove_t* me, I2C_HandleTypeDef* p_i2c_base);
LCD_status_t LCD_BACKLIGHT_set_rgb(lcd_grove_t* me, uint8_t ui8_red, uint8_t ui8_green, uint8_t ui8_blue);
LCD_status_t LCD_BACKLIGHT_set_color(lcd_grove_t* me, lcd_backlight_t color);


/*---------------------------------------------------------------------------*/
/*-------------------- Prop�tipos de fun��es privadas -----------------------*/

LCD_status_t lcd_command(lcd_grove_t* me, uint8_t ui8_command);
LCD_status_t lcd_send_byte(lcd_grove_t* me, uint8_t* p_ui8_buffer, uint8_t ui8_size, uint32_t ui32_timeout);
void lcd_commandCGRAM(lcd_grove_t* me,uint8_t value);
LCD_status_t set_reg(lcd_grove_t* me, uint8_t ui8_address, uint8_t ui8_data, uint32_t ui32_timeout);


/*---------------------------------------------------------------------------*/
/*------------------------- Fun��es p�blicas --------------------------------*/

LCD_status_t LCD_init(lcd_grove_t* me, I2C_HandleTypeDef* p_i2c_base, lcd_row_t rows)
{
	LCD_status_t return_value;

	// Para que esse ASSERT??? Curioso?
	// Para que esse ASSERT??? Curioso?
	// Ver t�picos 10 - Programming by contract e 13 - Assert em C do arquivo "Help"
	// disponibilizado na pasta "Material de consulta\Documenta��o de c�digo\code_example"
	assert(p_i2c_base->Instance != NULL);

	// Initialize device backlight
	me->b_lcd_init 		= true;
	me->ui8_address_lcd	= LCD_ADDRESS;
	me->ui8_timeout		= 5;
	me->p_i2c_base		= p_i2c_base;
    // Send function set command sequence
    lcd_command(me, LCD_FUNCTION_SET | rows);
    HAL_Delay(1);  // wait more than 4.1ms

    // turn the display on with no cursor or blinking default
    lcd_command(me, LCD_DISPLAY_CONTROL | LCD_DISPLAY_ON | LCD_CURSOR_OFF | LCD_BLINK_OFF);

    // clear it off
    LCD_clear(me);

    // Initialize to default text direction (for romance languages)
    // set the entry mode
    lcd_command(me, LCD_ENTRY_MODE_SET | LCD_ENTRY_LEFT | LCD_ENTRY_SHIFT_DECREMENT);

    LCD_home(me);

    // backlight init
    LCD_BACKLIGHT_init(me, me->p_i2c_base);
}


LCD_status_t LCD_home(lcd_grove_t* me)
{
	LCD_status_t return_value;

	return_value = lcd_command(me, LCD_RETURN_HOME);        // set cursor position to zero
    HAL_Delay(2);        			// this command takes a long time!

    return return_value;
}


LCD_status_t LCD_clear(lcd_grove_t* me)
{
	LCD_status_t return_value;

	return_value = lcd_command(me, LCD_CLEAR_DISPLAY);     // clear display, set cursor position to zero
    //HAL_Delay(2);          								// this command takes a long time!
	LCD_set_cursor(me, 0, 0);

    return return_value;
}


LCD_status_t LCD_set_cursor(lcd_grove_t* me, uint8_t col, uint8_t row)
{
	uint8_t ui8_tx_buffer[2];

	ui8_tx_buffer[0] = 0x00;
	ui8_tx_buffer[1] = (row == 0 ? col|0x80 : col|0xc0);

    return lcd_send_byte(me, ui8_tx_buffer, 2, me->ui8_timeout);
}


LCD_status_t LCD_write_char(lcd_grove_t* me, uint8_t ui8_value)
{
	uint8_t ui8_tx_buffer[2] = {0x40, ui8_value};

    return lcd_send_byte(me, ui8_tx_buffer, 2, me->ui8_timeout);
}


void LCD_print(lcd_grove_t me, char* message, uint8_t col, uint8_t row  )
{
	LCD_set_cursor(&me, col, row);
	for(uint8_t i = 0; i < strlen(message); i++)
	{
		LCD_write_char(&me, message[i]);
	}
}

LCD_status_t LCD_BACKLIGHT_init(lcd_grove_t* me, I2C_HandleTypeDef* p_i2c_base)
{
	LCD_status_t return_value;

	// Para que esse ASSERT??? Curioso?
	// Para que esse ASSERT??? Curioso?
	// Ver t�picos 10 - Programming by contract e 13 - Assert em C do arquivo "Help"
	// disponibilizado na pasta "Material de consulta\Documenta��o de c�digo\code_example"
	assert(p_i2c_base->Instance != NULL);

	// Initialize device backlight
	me->b_backlight_init 	= true;
	me->ui8_address_rbg 	= RGB_ADDRESS;
	me->ui8_timeout			= 5;
	me->p_i2c_base			= p_i2c_base;

	// backlight init
	return_value = set_reg(me, REG_MODE1, 0x00, me->ui8_timeout);

	// set LEDs controllable by both PWM and GRPPWM registers
	if(return_value == LCD_OK)
	{
		return_value = set_reg(me, REG_OUTPUT, 0xFF, me->ui8_timeout);
	}

	// set MODE2 values
	// 0010 0000 -> 0x20  (DMBLNK to 1, ie blinky mode)
	if(return_value == LCD_OK)
	{
		return_value = set_reg(me, REG_MODE2, 0x20, me->ui8_timeout);
	}

	if(return_value != LCD_OK)
	{
		me->b_backlight_init 	= false;
	}

	return return_value;
}


LCD_status_t LCD_BACKLIGHT_set_rgb(lcd_grove_t* me, uint8_t ui8_red, uint8_t ui8_green, uint8_t ui8_blue)
{
	LCD_status_t return_value;

	return_value = set_reg(me, REG_RED, ui8_red, me->ui8_timeout);

	if(return_value == LCD_OK)
	{
		return_value = set_reg(me, REG_GREEN, ui8_green, me->ui8_timeout);
	}

	if(return_value == LCD_OK)
	{
		return_value = set_reg(me, REG_BLUE, ui8_blue, me->ui8_timeout);
	}

	return return_value;
}


LCD_status_t LCD_BACKLIGHT_set_color(lcd_grove_t* me, lcd_backlight_t color)
{
	return LCD_BACKLIGHT_set_rgb(me, ui8_color_define[color][0], ui8_color_define[color][1],
			ui8_color_define[color][2]);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void LCD_createChar(lcd_grove_t* me, uint8_t location, uint8_t charmap[])
{
    location &= 0x7; // we only have 8 locations 0-7
    lcd_commandCGRAM(me,LCD_SET_CGRAM_ADDR | (location << 3));

    uint8_t dta[9];
    dta[0] = LCD_SET_CGRAM_ADDR;
    for(int i=0; i<8; i++)
    {
        dta[i+1] = charmap[i];
    }
    lcd_send_byte(me, dta, 9, me->ui8_timeout);
}


/*---------------------------------------------------------------------------*/
/*------------------------- Fun��es privadas --------------------------------*/

void lcd_commandCGRAM(lcd_grove_t* me,uint8_t value)
{
	uint8_t ui8_tx_buffer[2] = {0x00, value};
    lcd_send_byte(me, ui8_tx_buffer, 2, me->ui8_timeout);
}



LCD_status_t lcd_command(lcd_grove_t* me, uint8_t ui8_command)
{
	uint8_t ui8_tx_buffer[2] = {0x00, ui8_command};

    return lcd_send_byte(me, ui8_tx_buffer, 2, me->ui8_timeout);
}


LCD_status_t lcd_send_byte(lcd_grove_t* me, uint8_t* p_ui8_buffer, uint8_t ui8_size, uint32_t ui32_timeout)
{
	// Para que esse ASSERT??? Curioso?
	// Ver t�picos 10 - Programming by contract e 13 - Assert em C do arquivo "Help"
	// disponibilizado na pasta "Material de consulta\Documenta��o de c�digo\code_example"
	assert(me->b_lcd_init);

    return HAL_I2C_Master_Transmit(me->p_i2c_base, me->ui8_address_lcd, p_ui8_buffer, ui8_size, ui32_timeout);
}



LCD_status_t set_reg(lcd_grove_t* me, uint8_t ui8_address, uint8_t ui8_data, uint32_t ui32_timeout)
{
	uint8_t ui8_tx_buffer[2];

	// Para que esse ASSERT??? Curioso?
	// Ver t�picos 10 - Programming by contract e 13 - Assert em C do arquivo "Help"
	// disponibilizado na pasta "Material de consulta\Documenta��o de c�digo\code_example"
	assert(me->b_backlight_init);

	ui8_tx_buffer[0] = ui8_address;
	ui8_tx_buffer[1] = ui8_data;

	return HAL_I2C_Master_Transmit(me->p_i2c_base, me->ui8_address_rbg, ui8_tx_buffer, 0x02, ui32_timeout);
}
