//*---------------------------------------------------------------------------*/
/** \file Access.c
 * \page Access Access.c
 *
 * \author Breno Alves
 * \n breno.alves@ee.ufcg.edu.br

 * \version 0.1
 * \date Creation: 01 mar 2018.
 * \date Last Update: 15 mar 2018.
 *
 * *****************************************************************************
*/

/*---------------------------------------------------------------------------*/
/*----------------------------- Includes ------------------------------------*/

#include <Access.h>
#include <string.h>

/*---------------------------------------------------------------------------*/
/*--------------------- Tipos e vari�veis privadas --------------------------*/

typedef struct
{
	userInfo_t userSession;
	statusAccess_t statusSession;
	resultAccess_t resultSession;

}accessSession_t;

accessSession_t session;
static uint8_t numberOfUsers;
static uint8_t numberOfAccess;
userInfo_t (*usersList)[];
logAccess_t (*log)[];


/*---------------------------------------------------------------------------*/
/*-------------------- Prop�tipos de fun��es p�blicas -----------------------*/

void Access_Init();
void Access_ReadCPF(uint8_t pressedKey);
void Access_ReadPassword(uint8_t pressedKey);
statusAccess_t Access_GetStatus();
resultAccess_t Access_GetResult();
char * Access_GetPartialCPF();
char * Access_GetUserName();


/*---------------------------------------------------------------------------*/
/*-------------------- Prop�tipos de fun��es privadas -----------------------*/

void Access_CreateUserDefault();
int8_t Access_FindUserByCPF(char* sessionUserCPF);
uint8_t Access_CheckUserPassword(char* sessionUserPassword);
void BeginSession();
void Access_UpdateLog();


/*---------------------------------------------------------------------------*/
/*------------------------- Fun��es p�blicas --------------------------------*/

/**
 * \brief Fun��o Access_Init()
 * \description Fun��o que inicializa as vari�veis do sistema de acesso.
 * \param
 * \return
 */
void Access_Init()
{
	userInfo_t users[MAX_USERS];
	usersList = &users;

	logAccess_t l[MAX_LOG];
	log = &l;

	numberOfUsers = 0x00;
	numberOfAccess = 0x00;

	BeginSession();

	// Povoando o banco de usu�rios
	Access_CreateUserDefault();
};


/**
 * \brief Fun��o Access_GetPartialCPF()
 * \description Fun��o que retorna o valor parcial do cpf
 * \param
 * \return Uma string com os digitos do cpf digitados at� o momento
 */
char * Access_GetPartialCPF()
{
	return session.userSession.CPF;
}


/**
 * \brief Fun��o Access_GetUserName()
 * \description Fun��o que retorna o username do usu�rio
 * \param
 * \return Uma string com o nome do usu�rio
 */
char * Access_GetUserName()
{
	return session.userSession.Name;
}


/**
 * \brief Fun��o Access_GetStatus()
 * \description Fun��o que retorna o status da sess�o.
 * \param
 * \return Um enum do tipo statusAccess_t com o status da sess�o.
 */
statusAccess_t Access_GetStatus()
{
	return session.statusSession;
}


/**
 * \brief Fun��o Access_GetResult()
 * \description Fun��o que retorna o resultado de alguma opera��o.
 * \param
 * \return Um enum do tipo resultAccess_t com o resultado de uma opera��o.
 */
resultAccess_t Access_GetResult()
{
	return session.resultSession;
}


/**
 * \brief Fun��o Access_ReadCPF()
 * \description Fun��o que realzia a leitura dos d�gitos do CPF.
 * \param Um uint8_t contendo o ultimo d�gito do CPF informado.
 * \return
 */
void Access_ReadCPF(uint8_t pressedKey)
{
	session.resultSession = Access_No_result;

	uint8_t index = strlen(session.userSession.CPF);
	session.userSession.CPF[index] = pressedKey;

	if(index == CPF_SIZE-1)
	{
		int8_t userIndex = Access_FindUserByCPF(&(session.userSession.CPF));

		if(userIndex == -1)
		{
			memset(session.userSession.CPF,0,strlen(session.userSession.CPF));
			session.statusSession = Waiting_CPF_status;
			session.resultSession = Access_Denied_result;
		}
		else
		{
			session.userSession = (*usersList)[userIndex];
			session.statusSession = Waiting_Password_status;
		}
	}
	else
	{
		session.statusSession = Waiting_CPF_status;
	}
}


/**
 * \brief Fun��o Access_ReadPassword()
 * \description Fun��o que realiza a leitura dos d�gitos da senha.
 * \param Um uint8_t contendo o ultimo d�gito da senha informado.
 * \return
 */
void Access_ReadPassword(uint8_t pressedKey)
{
	session.resultSession = Access_No_result;

	static char enteredPassword[PASSWORD_SIZE];

	uint8_t index = strlen(enteredPassword);
	enteredPassword[index] = pressedKey;

	if(index == PASSWORD_SIZE - 1)
	{
		if( Access_CheckUserPassword(enteredPassword))
		{
			Access_UpdateLog();
			BeginSession();
			memset(enteredPassword,0,strlen(enteredPassword));
			session.resultSession = Access_Allowed_result;

		}
		else
		{
			BeginSession();
			memset(enteredPassword,0,strlen(enteredPassword));
			session.resultSession = Access_Denied_result;
		}
	}
	else
	{
		session.statusSession = Waiting_Password_status;
	}
}


/*---------------------------------------------------------------------------*/
/*------------------------- Fun��es privadas --------------------------------*/

/**
 * \brief Fun��o Access_FindUserByCPF()
 * \description Verifica se o CPF digitado est� cadastrado
 * \param Um char*  contendo o o CPF do usu�rio da sess�o.
 * \return O index do usu�rio na lista de usu�rios caso esteja cadastrado ou -1 se n�o.
 */
int8_t Access_FindUserByCPF(char* sessionUserCPF)
{

	for(uint8_t i = 0; i < numberOfUsers; i++ )
	{
		if( strcmp(sessionUserCPF , (*usersList)[i].CPF ) == 0)
		{
			return i;
		}
	}
	return -1;
}


/**
 * \brief Fun��o Access_CheckUserPassword()
 * \description Verifica se a senha est� correta.
 * \param Um char*  contendo o a senha do usu�rio da sess�o.
 * \return 0 se a senha estiver incorreta ou 1 se estiver correta.
 */
uint8_t Access_CheckUserPassword(char* sessionUserPassword )
{
	char pass[4];
	char* pass_ptr = &pass;
	strcpy(pass_ptr, sessionUserPassword);

	for(uint8_t i = 0; i < PASSWORD_SIZE; i++)
	{
		if( pass[i] != session.userSession.Password[i])
		{
			return 0;
		}
	}
	return 1;
}


/**
 * \brief Fun��o BeginSession()
 * \description Inicializa as vari�veis da sess�o.
 * \param
 * \return
 */
void BeginSession()
{
	userInfo_t emptyUser = {"","",""};
	session.userSession = emptyUser;
	session.statusSession = Waiting_CPF_status;
	session.resultSession = Access_No_result;
}


/**
 * \brief Fun��o Access_CreateUserDefault()
 * \description Povoa a lista de usu�rios com usu�rios padr�o.
 * \param
 * \attention Deve ser alterada para que os dados sejam informados pelo usu�rio
 * \return
 */
void Access_CreateUserDefault()
{
	userInfo_t user1, user2, user3;

	user1.Name = "Batman";
	strcpy(user1.Password  , "1111");
	strcpy(user1.CPF , "11111111111");

	user2.Name = "Chaves";
	strcpy(user2.Password , "2222");
	strcpy(user2.CPF ,  "22222222222");

	user3.Name = "Silvio Santos";
	strcpy(user3.Password , "3333");
	strcpy(user3.CPF ,  "33333333333");

	(*usersList)[0] = user1; numberOfUsers++;
	(*usersList)[1] = user2; numberOfUsers++;
	(*usersList)[2] = user3; numberOfUsers++;

};


/**
 * \brief Fun��o Access_UpdateLog()
 * \description Atualiaza o log de acessos com o n�mero do acesso, o nome e a hora.
 * \param
 * \attention Deve ser alterada para que o log seja armazenado em um banco de dados
 * \bug N�o est� sendo poss�vel mostrar a hora pois � neces�rio implementar um driver para contar o tempo.
 * \return
 */
void Access_UpdateLog()
{
	//time_t t = time(NULL);
	(*log)[numberOfAccess].UserCPF = session.userSession.CPF;
	//(*log)[numberOfAccess].Time = *localtime(&t);
	numberOfAccess++;

	printf("%d - %s %s", numberOfAccess, session.userSession.Name,session.userSession.CPF);
}

