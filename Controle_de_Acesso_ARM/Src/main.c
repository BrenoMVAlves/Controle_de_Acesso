/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "keypad_4x4.h"
#include "retarget.h"
#include "grove_lcd_rgb_v2.h"
#include "Access.h"
#include "string.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/


uint8_t counterTeclado = 0;
uint8_t counterSerial = 0;
uint8_t counterLCD = 0;
uint8_t MAXteclado = 4;				// 4*50ms  = 200ms
uint8_t MAXserial = 5;				// 5*50ms  = 250ms
uint8_t MAXlcd = 10;				// 10*50ms = 500ms
uint8_t teclaPressionadaSerial, teclaPressionadaLCD;
lcd_grove_t lcd_grove;
uint8_t cont_teclaPressionada;
uint8_t statusAcesso;
uint8_t resultAcesso;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void PrintEntradaNegada();
void PrintEntradaPermitida();
void PrintCPF();
void PrintSenha();
void PrintStar();
void Bateria();

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM10_Init();
  MX_I2C1_Init();

  /* USER CODE BEGIN 2 */

  /* Redireciona as estruturas de entrada e sa�da do stdio para a serial */
	RetargetInit(&huart2);

  /* Inicializa��o do Tim10*/
	HAL_TIM_Base_Start_IT(&htim10);

  /* Inicializa LCD e Backlight */
	LCD_init(&lcd_grove, &hi2c1, LCD_ROW_02);


  /* Inicializa��o do Teclado Matricial*/
 	KEYPAD_4x4_Setup_t KEYPAD_4x4_setup;
 	KEYPAD_4x4_setup.ROW1.Pin  = Linha_1_Pin;
 	KEYPAD_4x4_setup.ROW2.Pin  = Linha_2_Pin;
 	KEYPAD_4x4_setup.ROW3.Pin  = Linha_3_Pin;
 	KEYPAD_4x4_setup.ROW4.Pin  = Linha_4_Pin;
 	KEYPAD_4x4_setup.ROW1.Port = Linha_1_GPIO_Port;
 	KEYPAD_4x4_setup.ROW2.Port = Linha_2_GPIO_Port;
 	KEYPAD_4x4_setup.ROW3.Port = Linha_3_GPIO_Port;
 	KEYPAD_4x4_setup.ROW4.Port = Linha_4_GPIO_Port;
 	KEYPAD_4x4_setup.COL1.Pin  = Coluna_1_Pin;
 	KEYPAD_4x4_setup.COL2.Pin  = Coluna_2_Pin;
 	KEYPAD_4x4_setup.COL3.Pin  = Coluna_3_Pin;
 	KEYPAD_4x4_setup.COL4.Pin  = Coluna_4_Pin;
 	KEYPAD_4x4_setup.COL1.Port = Coluna_1_GPIO_Port;
 	KEYPAD_4x4_setup.COL2.Port = Coluna_2_GPIO_Port;
 	KEYPAD_4x4_setup.COL3.Port = Coluna_3_GPIO_Port;
 	KEYPAD_4x4_setup.COL4.Port = Coluna_4_GPIO_Port;

 	KEYPAD_4x4_Init( &KEYPAD_4x4_setup);

 	Access_Init();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

 	//Bateria()
 	LCD_BACKLIGHT_set_rgb(&lcd_grove, 255, 255, 255);

 	PrintCPF();

 while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 64;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

uint8_t DecodedKeypad(KEYPAD_4x4_ButtonEnum_t dtecla)
{
	uint8_t tecla;
	switch((uint8_t)dtecla)
	{
		case KEYPAD_4x4_Button_1:
			tecla = '1';
			break;
		case KEYPAD_4x4_Button_2:
			tecla = '2';
			break;
		case KEYPAD_4x4_Button_3:
			tecla = '3';
			break;
		case KEYPAD_4x4_Button_4:
			tecla = '4';
			break;
		case KEYPAD_4x4_Button_5:
			tecla = '5';
			break;
		case KEYPAD_4x4_Button_6:
			tecla = '6';
			break;
		case KEYPAD_4x4_Button_7:
			tecla = '7';
			break;
		case KEYPAD_4x4_Button_8:
			tecla = '8';
			break;
		case KEYPAD_4x4_Button_9:
			tecla = '9';
			break;
		case KEYPAD_4x4_Button_A:
			tecla = 'A';
			break;
		case KEYPAD_4x4_Button_B:
			tecla = 'B';
			break;
		case KEYPAD_4x4_Button_C:
			tecla = 'C';
			break;
		case KEYPAD_4x4_Button_D:
			tecla = 'D';
			break;
		case KEYPAD_4x4_Button_0:
			tecla = '0';
			break;
		case KEYPAD_4x4_Button_HASH:
			tecla = '#';
			break;
		case KEYPAD_4x4_Button_STAR:
			tecla = '*';
			break;
		case KEYPAD_4x4_Button_NOPRESSED:
			tecla = 0;
			break;
		default:
			tecla = 0;
	}
	return tecla;
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM10)
	{
		if(counterTeclado == MAXteclado)
		{
			statusAcesso = Access_GetStatus();
			resultAcesso = Access_GetResult();
			uint8_t aux = DecodedKeypad(KEYPAD_4x4_GetPressedKey());

			if(aux) // Nova Leitura do teclado
			{
				teclaPressionadaSerial = aux;
				teclaPressionadaLCD = aux;

				if(statusAcesso == Waiting_CPF_status)
				{
					Access_ReadCPF(aux);
				}
				else if(statusAcesso == Waiting_Password_status)
				{
					Access_ReadPassword(aux);
				}
			}
			counterTeclado = 0;
		}
		if(counterSerial == MAXserial)
		{
			if(teclaPressionadaSerial)
			{
				//HAL_UART_Transmit(&huart2, &teclaPressionadaSerial, 1, HAL_MAX_DELAY);
				//HAL_UART_Transmit(&huart2, "\r\n", 2, HAL_MAX_DELAY);
				teclaPressionadaSerial = 0;
			}
			counterSerial = 0;
		}
		if(counterLCD == MAXlcd)
		{
			static uint8_t contaQuandoEntraAqui = 0;

			if(resultAcesso == Access_Denied_result  )
			{
				if(contaQuandoEntraAqui == MAXlcd/4)
				{
					PrintCPF();
				}
				else
				{
					PrintEntradaNegada();
					contaQuandoEntraAqui ++;
				}
			}
			else if(resultAcesso == Access_Allowed_result  )
			{
				if(contaQuandoEntraAqui == MAXlcd/4)
				{
					PrintCPF();
				}
				else
				{
					PrintEntradaPermitida();
					contaQuandoEntraAqui ++;
				}

			}
			else if(resultAcesso == Access_No_result  )
			{
				contaQuandoEntraAqui = 0;
				if(statusAcesso == Waiting_CPF_status)
				{
					PrintCPF();
				}
				else if(statusAcesso == Waiting_Password_status)
				{
					PrintSenha();
					if(teclaPressionadaLCD)
					{
						PrintStar();
						teclaPressionadaLCD = 0;
					}
				}
			}

			counterLCD = 0;
		}
		counterTeclado++;
		counterSerial++;
		counterLCD++;
	}
}

void MyClearLCD()
{
	LCD_print(lcd_grove, "                ", 0, 0);
	LCD_print(lcd_grove, "                ", 0, 1);
	LCD_set_cursor(&lcd_grove, 0, 0);
}

void PrintEntradaNegada()
{
	MyClearLCD();
	LCD_print(lcd_grove, "Acesso Negado", 0, 0);
}

void PrintEntradaPermitida()
{
	MyClearLCD();
	LCD_print(lcd_grove, "Entrada", 3, 0);
	LCD_print(lcd_grove, "Autorizada", 2, 1);
}

void PrintCPF()
{
	char* cpf = Access_GetPartialCPF();

	if( strlen(cpf) < 11)
	{
		if( strlen(cpf) == 0)
		{
			MyClearLCD();
		}
		LCD_print(lcd_grove, "Digite seu CPF:", 0, 0);
		LCD_print(lcd_grove, cpf, 0, 1);
	}
}

void PrintSenha()
{
	//Mensagem para o usuario
	char* mgs_oi = "Oi,";
	char* msg_nome = Access_GetUserName();

	LCD_print(lcd_grove,mgs_oi, 0, 0);
	LCD_print(lcd_grove,msg_nome, 3, 0);
	LCD_print(lcd_grove, "Senha: ", 0, 1);

}

void PrintStar()
{

	//Logia para preencher com *
	static int8_t count = 0;
	char senhaStar[] = "    ";
	char c = '*';

	if( count == 0)
	{
		MyClearLCD();
	}
	if(count > 0 && count < 4)
	{
		memset(senhaStar,c,count);
		LCD_print(lcd_grove, senhaStar, 7, 1);
	}
	if(count == 4)
	{
		count = -1;
		strcpy(senhaStar,"    ");
	}
	count++;

}

void Bateria()
{
	uint8_t charged_battery[8] = {
 		0b01110,
		0b01110,
		0b11111,
		0b11111,
		0b11111,
		0b11111,
		0b11111,
		0b11111

 	};

 	uint8_t battery_80[8] = {
 		0b01110,
 		0b01010,
		0b10001,
		0b11111,
		0b11111,
		0b11111,
		0b11111,
		0b11111
	};

 	uint8_t battery_40[8] = {
 		0b01110,
		0b01010,
		0b10001,
		0b10001,
		0b10001,
		0b11111,
		0b11111,
		0b11111
 	};

 	uint8_t low_battery[8] = {
 		0b01110,
 		0b01010,
 		0b10001,
 		0b10001,
 		0b10001,
 		0b10001,
 		0b10001,
 		0b11111
 	};
 	//LCD_print(lcd_grove, mensagem, 2, 0);
 	//LCD_createChar(&lcd_grove,0x00, charged_battery);
 	//LCD_createChar(&lcd_grove,0x01, battery_80);
 	//LCD_createChar(&lcd_grove,0x02, battery_40);
 	//LCD_createChar(&lcd_grove,0x03, low_battery);

 	 /* C�DIGO PARA MOSTRAR A BATERIA
 			 LCD_set_cursor(&lcd_grove, 15, 0);
 			 LCD_write_char(&lcd_grove, 0x00);
 			 HAL_Delay(500);
 			 LCD_set_cursor(&lcd_grove, 15, 0);
 			 LCD_write_char(&lcd_grove, 0x01);
 			 HAL_Delay(500);
 			 LCD_set_cursor(&lcd_grove, 15, 0);
 			 LCD_write_char(&lcd_grove, 0x02);
 			 HAL_Delay(500);
 			 LCD_set_cursor(&lcd_grove, 15, 0);
 			 LCD_write_char(&lcd_grove, 0x03);
 			 HAL_Delay(500);
 	  */
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
