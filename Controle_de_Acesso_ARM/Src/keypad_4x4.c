/*---------------------------------------------------------------------------*/
/** \file keypad.c
 * \page keypadc keypad_4x4.c
 * 
 * \author Breno Alves
 * \n breno.alves@ee.ufcg.edu.br
 * \author Inaie ALves
 * \n inaie.soares@ee.ufcg.edu.br
 * \author Vinicius Freire  
 * \n vinicius.bezerra@ee.ufcg.edu.br
 * \version 0.1
 * \date Creation: 14 nov 2017.
 * \date Last Update: 23  nov 2017.
 *
 * *****************************************************************************
 *
 *\section debouncecode Debounce Code
 * To correct the error generated "debounce" was created the class:
 *
 *\code
 * uint8_t Debounce(GPIO_TypeDef *Port, uint16_t Pin)
 *
 *	 uint8_t valTemp = 0;
 *
 *
 * 	 uint8_t debounceTime = 22; 	// 22 cycles
 *	 while (debounceTime > 0)
 *	 {
 *		 // check if button bounced
 *		 if (valTemp == !HAL_GPIO_ReadPin(Port, Pin))
 *		 {
 *			 debounceTime--;
 *		 }
 *	 }
 *	 return debounceTime <= 0 ? 1:0;
 *
 *}
 *
 *\endcode 
 *
 */

/* Includes ------------------------------------------------------------------*/
#include <keypad_4x4.h>
#include "gpio.h"

/* Private functions declaration -----------------------------------------------*/
uint8_t KEYPAD_4x4_ReadButton(void);
void KEYPAD_4x4_SetRow(uint8_t);
uint8_t KEYPAD_4x4_checkColumn(uint8_t);
uint8_t Debounce(GPIO_TypeDef*, uint16_t);

/* Private Variables ------------------------------------------------------------------*/
uint8_t KEYPAD_4X4_Buttons[4][4] = {
	{0x01, 0x02, 0x03, 0x0C},
	{0x04, 0x05, 0x06, 0x0D},
	{0x07, 0x08, 0x09, 0x0E},
	{0x0A, 0x00, 0x0B, 0x0F},
};
static KEYPAD_4x4_Setup_t keypadSetup;
static KEYPAD_4x4_ButtonEnum_t pressedKey = KEYPAD_4x4_Button_NOPRESSED;



/* Public Functions ------------------------------------------------------------------*/

void KEYPAD_4x4_Init(KEYPAD_4x4_Setup_t *_setup )
{
		keypadSetup = *_setup;
}

KEYPAD_4x4_ButtonEnum_t KEYPAD_4x4_GetPressedKey(void)
{
	/* Get keypad status */
	pressedKey = (KEYPAD_4x4_ButtonEnum_t)KEYPAD_4x4_ReadButton();

	KEYPAD_4x4_ButtonEnum_t temp = pressedKey;

	/* Reset keypad status */
	pressedKey = KEYPAD_4x4_Button_NOPRESSED;

	return temp;
}



/* Private Functions ------------------------------------------------------------------*/

/**
 * \brief Private Function
 * Function that returns the reading of the pressed button on the keyboard
 * \post Your readings are used to solve the Bounce problem
 * \return The button pressed, if there is any
 *
 */
uint8_t KEYPAD_4x4_ReadButton(void)
{
	uint8_t check;
	for(uint8_t row = 1; row <= 4; row++ )
	{
		/* Set row to LOW*/
		KEYPAD_4x4_SetRow(row);
		/* Check column */
		check = KEYPAD_4x4_checkColumn(row);
		if (check != KEYPAD_NO_PRESSED) {
			return check;
		}
	}
	/* Not pressed */
	return KEYPAD_NO_PRESSED;

}

/**
 * \brief Private Function
 * This function sets the line to be used as a comparison for logic level
 * \n to find which button was pressed.
 * \param uint8_t row - Line index
 */
void KEYPAD_4x4_SetRow(uint8_t row)
{
	//Set rows to HIGH
	HAL_GPIO_WritePin(keypadSetup.ROW1.Port, keypadSetup.ROW1.Pin, SET);
	HAL_GPIO_WritePin(keypadSetup.ROW2.Port, keypadSetup.ROW2.Pin, SET);
	HAL_GPIO_WritePin(keypadSetup.ROW3.Port, keypadSetup.ROW3.Pin, SET);
	HAL_GPIO_WritePin(keypadSetup.ROW4.Port, keypadSetup.ROW4.Pin, SET);

	//Set the tested row to LOW
	if (row == 1) {
		HAL_GPIO_WritePin(keypadSetup.ROW1.Port, keypadSetup.ROW1.Pin, RESET);
	}
	else if (row == 2) {
		HAL_GPIO_WritePin(keypadSetup.ROW2.Port, keypadSetup.ROW2.Pin, RESET);
	}
	else if (row == 3) {
		HAL_GPIO_WritePin(keypadSetup.ROW3.Port, keypadSetup.ROW3.Pin, RESET);
	}
	else if (row == 4) {
		HAL_GPIO_WritePin(keypadSetup.ROW4.Port, keypadSetup.ROW4.Pin, RESET);
	}
}
/**
 * \brief Private Function
 *
 * \param uint8_t row - Line index
 */
uint8_t KEYPAD_4x4_checkColumn(uint8_t row)
{

	/* Scan Col 1 */
	if (!HAL_GPIO_ReadPin(keypadSetup.COL1.Port, keypadSetup.COL1.Pin))
	{
		if(Debounce(keypadSetup.COL1.Port,keypadSetup.COL1.Pin))
		{
			return KEYPAD_4X4_Buttons[row-1][0];
		}
	}
	/* Scan Col 2 */
	else if (!HAL_GPIO_ReadPin(keypadSetup.COL2.Port, keypadSetup.COL2.Pin))
	{
		if(Debounce(keypadSetup.COL2.Port,keypadSetup.COL2.Pin))
		{
			return KEYPAD_4X4_Buttons[row-1][1];
		}
	}
	/* Scan Col 3 */
	else if (!HAL_GPIO_ReadPin(keypadSetup.COL3.Port, keypadSetup.COL3.Pin))
	{
		if(Debounce(keypadSetup.COL3.Port,keypadSetup.COL3.Pin))
		{
			return KEYPAD_4X4_Buttons[row-1][2];
		}
	}
	/* Scan Col 4 */
	else if (!HAL_GPIO_ReadPin(keypadSetup.COL4.Port, keypadSetup.COL4.Pin))
	{
		if(Debounce(keypadSetup.COL4.Port,keypadSetup.COL4.Pin))
		{
			return KEYPAD_4X4_Buttons[row-1][3];
		}
	}
	/* Not pressed */

	return (uint)KEYPAD_NO_PRESSED;
}
/**
 * \brief Private Function
 * Function that solves the keyboard Bounce problem
 * \param GPIO_TypeDef *Port -
 * \param uint16_t Pin -
 * \return - An integer that indicates when the Bounce problem has been resolved
 */
uint8_t Debounce(GPIO_TypeDef *Port, uint16_t Pin)
{
	 uint8_t valTemp = 0;
	 uint8_t debounceTime = 22; 	// 22 cycles
	 while (debounceTime > 0)
	 {
		 // check if button bounced
		 if (valTemp == !HAL_GPIO_ReadPin(Port, Pin))
		 {
			 debounceTime--;
		 }
	 }
	 return debounceTime <= 0 ? 1:0;

}




/*
 * keypad_4x4.c
 *
 *  Created on: 14 de nov de 2017
 *      Author: TEEE_IIoT
 */


