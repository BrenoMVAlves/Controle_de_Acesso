Projeto de desenvolvimento de um sistema de Controle de Acesso baseado nos conceitos de Internet das Coisas.


Visão Geral

    O sistema compreende de um módulo, chamado módulo I/OT, que deve ser intalado a porta dos ambientes e realizará o controle de acesso. 
    Este módulo tem conectividade com uma aplicação web cliente-servidor, na qual são cadastrados os usuários e hitsóriadas as informações de acesso.

Lista de Materiais 

    HARDUWARE:
        Microcontrolador STM32F402RE com processador ARM
        Módulo Wifi ESP8266
        Teclado Matricial 4x4
        Relé
        Visor LCD
    SOFTWARE
        STM32Cube MX
        TrueStudio
        ArduinoIDE
        NodeRed
        
Conteúdo:

    Código de configuração do microcontrolador.
    Código Principal do microcontrolador
    Código de comunicação do ESP8266 com o NodeRED via MQTT.
    Biblioteca C para o Visor LCD.
    Biblioteca C para o teclado matricial 4x4 com documentação.
    Biblioteca C para o Controle de Acesso (Autenticação).

\Attention

Projeto Incompleto