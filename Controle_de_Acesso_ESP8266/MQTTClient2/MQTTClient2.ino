  //####################################################################################
// Universidade Federal de Campina Grande
// Centro de Engenharia Elétrica e Informática
// Graduação em Engenharia Elétrica
//
// Professor : Rafael Bezerra Correia Lima
// Engenheiro: Christian Charles Dias
//
// Disciplina: TEEE - Industrial Internet of Things - 2017.2
//
// Descrição: Utilização do ESP8266 com protocolo MQTT
//
//####################################################################################

//************************************************************************************
// Bibliotecas
#include <ESP8266WiFi.h>
#include <PubSubClient.h>   // Documentação https://pubsubclient.knolleary.net/api.html


//************************************************************************************
// Configuração da rede
const char* SSID = "LIEC_Wireless3";                // Nome da rede Wi-Fi
const char* PASSWORD = "0987ABCDEF";                // senha da rede Wi-Fi


//************************************************************************************
// Configuração para MQTT
//const char* BROKER_MQTT = "broker.hivemq.com";      // IP/host do broker
const char* BROKER_MQTT = "150.165.52.188";         // IP/host do broker
int BROKER_PORT = 1883;                             // Porta do broker

const char* CLIENT_ID = "LIEC_IIoT_045";             // ID do client único para cada cliente conectado ao BROKER
const char* TOPIC_PUBLISH = "LIEC/IIoT/Number";     // Tópicos para PUBLISH
const char* TOPIC_SUBSCRIBE = "LIEC/IIoT/Message";  // Tópicos para SUBSCRIBE

WiFiClient espClient;                       // Instancia objeto espClient
PubSubClient MQTT(espClient);               // instancia objeto mqtt


//************************************************************************************
// Variáveis GLOBAIS (are devil)
int delayTime = 2000;                       // Publica a cada x ms
int lastTime  = 0;                          // Tempo do envio da última mensagem
int number_msg = 1;                         // Número da mensagem


//************************************************************************************
// Protótipos de funções
void initWiFi();                            // Se conecta a rede Wi-Fi
void initMQTT();                            // Inicializa configurações do MQTT


//************************************************************************************
// setup
void setup()
{
  Serial.begin(115200);                     // Inicializa UART
  initWiFi();                               // Se conecta a rede Wi-Fi
  initMQTT();                               // Inicializa configurações do MQTT
}


//************************************************************************************
// Loop principal
void loop()
{
  if (!MQTT.connected())
  {
    reconnectMQTT();                          // Reconecta ao Broker
  }

  reconnectWiFi();                            // Reconecta caso necessário
  MQTT.loop();                                // Deve ser chamado regularmente. Tratamento de mensagens recebidas

  // Publica mensagem a cada delayTime
  if (millis() - lastTime > delayTime)
  {
    String msg = "Mensagem nº: " + String(number_msg);
    char mqtt_message[50];
    msg.toCharArray(mqtt_message, sizeof(mqtt_message));  // Converte para array de char

    if (MQTT.publish(TOPIC_PUBLISH, mqtt_message))
    {
      lastTime = millis();                      // Salva tempo de envio da última mensagem
      Serial.print("Mensagem nº: ");
      Serial.println(number_msg);
      number_msg++;
    }
    else
    {
      Serial.println("Falha no PUBLISH");
    }
  }
}


//************************************************************************************
// Função para inicializa e se conectar a rede Wi-Fi
void initWiFi()
{
  delay(10);
  Serial.println("Conectando-se em: " + String(SSID));
  WiFi.begin(SSID, PASSWORD);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100);
    Serial.print(".");
  }

  Serial.println();
  Serial.print("Conectado na Rede " + String(SSID) + " | IP => ");
  Serial.println(WiFi.localIP());
}


//************************************************************************************
// Funcão para inicializar MQTT
void initMQTT()
{
  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
  MQTT.setCallback(mqtt_callback);
}


//************************************************************************************
// Função que recebe as mensagens publicadas
void mqtt_callback(char* topic, byte* payload, unsigned int length)
{
  String message;

  for (int i = 0; i < length; i++)
  {
    char c = (char)payload[i];
    message += c;
  }

  Serial.println("Tópico => " + String(topic) + " | Valor => " + String(message));
  Serial.flush();
}


//************************************************************************************
// Função para se reconectar ao Broker
void reconnectMQTT()
{
  while (!MQTT.connected())
  {
    Serial.println("Tentando se conectar ao Broker MQTT: " + String(BROKER_MQTT));

    if (MQTT.connect(CLIENT_ID))
    {
      Serial.println("Conectado");
      MQTT.subscribe(TOPIC_SUBSCRIBE);
    }
    else
    {
      Serial.println("Falha ao Reconectar");
      Serial.println("Tentando se reconectar em 2 segundos");
      delay(2000);
    }
  }
}


//************************************************************************************
// Função para se reconectar a rede Wi-Fi
void reconnectWiFi()
{
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100);
    Serial.print(".");
  }
}
